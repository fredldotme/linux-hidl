# HIDL for linux

This builds and generates HIDL headers and binaries.

This is work in progress, expect bugs and failures!

### How it works

HIDL/treble uses a server-client format all using androids binder, and all vendor libraries does only depend on a
limited set of libraries from /system, this makes it easy for us to create a enviroment with the needed services and
libraries the vendor services need. Also since they use binder we can call any hardware services from glibc without
using libhybris. We can also easily support new devices since all that is needed is treble support and to rebuild the 
kernel with the reqired configs and our initrd.


### How to test

You will need to mount /vendor partition and mount/provide /system (android 9.0)

Disable all inits from /system, we dont need any of these at this moment. (rm or mv /system/etc/init)

Start the init binary built from this project.
Start hwservicemanager built from this project.
Start vendor service you need (example: /vendor/bin/hw/android.hardware.vibrator@1.1-service)
Run hw tests (example: hwtest_vibrator)


LL-NDK includes the following libraries: libEGL.so, libGLESv1_CM.so, libGLESv2.so, libGLESv3.so, libandroid_net.so, libc.so, libdl.so, liblog.so, libm.so, libnativewindow.so, libneuralnetworks.so, libsync.so, libvndksupport.so, and libvulkan.so, 




