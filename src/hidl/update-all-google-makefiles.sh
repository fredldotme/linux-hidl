#!/bin/bash

# TODO(b/35570956): replace makefile generation with something like 'hidl_interface' in a Soong module

../libhidl/update-makefiles.sh
../hardware/interfaces/update-makefiles.sh
../frameworks/hardware/interfaces/update-makefiles.sh
../system/hardware/interfaces/update-makefiles.sh

#$ANDROID_BUILD_TOP/system/tools/hidl/test/vendor/update-makefile.sh
