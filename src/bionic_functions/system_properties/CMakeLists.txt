add_library(system_properties STATIC
        context_node.cpp
        contexts_split.cpp
        contexts_serialized.cpp
        prop_area.cpp
        prop_info.cpp
        system_properties.cpp
)

target_include_directories(system_properties PUBLIC include)
target_include_directories(system_properties PRIVATE ../include ..)
target_link_libraries(system_properties libpropertyinfoparser libasync_safe)
set_property(TARGET system_properties PROPERTY POSITION_INDEPENDENT_CODE ON)

