/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "log.h"

#include <fcntl.h>
#include <linux/audit.h>
#include <string.h>
#include <cstdarg>

#include <android-base/logging.h>

namespace android {
namespace init {

void InitKernelLogging(char* argv[]) {
    android::base::InitLogging(argv, &android::base::KernelLogger);
}

int selinux_klog_callback(int type, const char *fmt, ...) {
    android::base::LogSeverity severity = android::base::ERROR;
    char buf[1024];
    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, sizeof(buf), fmt, ap);
    va_end(ap);
    android::base::KernelLogger(android::base::MAIN, severity, "selinux", nullptr, 0, buf);
    return 0;
}

}  // namespace init
}  // namespace android
